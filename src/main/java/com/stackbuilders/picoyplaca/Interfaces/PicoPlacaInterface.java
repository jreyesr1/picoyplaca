package com.stackbuilders.picoyplaca.Interfaces;

import com.stackbuilders.picoyplaca.Entity.Response;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author JREYESR
 */
public interface PicoPlacaInterface {
    
    //Method to set and get a List with Days and numbers of Plates to make the restriction
    public HashMap<Integer, ArrayList<Integer>> getDaysPlatesNumbers();    
    //Method to predict Pico y Placa
    public Response canBeOnRoad(String licencePlateNumber, String date, String time);
}
