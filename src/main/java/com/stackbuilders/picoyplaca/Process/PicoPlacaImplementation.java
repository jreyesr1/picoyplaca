package com.stackbuilders.picoyplaca.Process;

import com.stackbuilders.picoyplaca.Entity.Response;
import com.stackbuilders.picoyplaca.Interfaces.PicoPlacaInterface;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author JREYESR
 */
public class PicoPlacaImplementation implements PicoPlacaInterface {

    @Override
    public HashMap<Integer, ArrayList<Integer>> getDaysPlatesNumbers() {
        //Set days and Car Plates..
        HashMap<Integer, ArrayList<Integer>> daysPlatesNumbers = new HashMap<Integer, ArrayList<Integer>>();
        daysPlatesNumbers.put(1, new ArrayList<Integer>(Arrays.asList(1, 2)));
        daysPlatesNumbers.put(2, new ArrayList<Integer>(Arrays.asList(3, 4)));
        daysPlatesNumbers.put(3, new ArrayList<Integer>(Arrays.asList(5, 6)));
        daysPlatesNumbers.put(4, new ArrayList<Integer>(Arrays.asList(7, 8)));
        daysPlatesNumbers.put(5, new ArrayList<Integer>(Arrays.asList(9, 0)));

        return daysPlatesNumbers;
    }

    @Override
    public Response canBeOnRoad(String licencePlateNumber, String date, String time) {
        HashMap<Integer, ArrayList<Integer>> daysPlatesNumbers = getDaysPlatesNumbers();
        //Check Valid inputs with regular expressions (Time)
        if (time.matches("^(([0-9])|([0-1][0-9])|([2][0-3]))(:(([0-9])|([0-5][0-9])))?$")) {
            LocalTime lTime = LocalTime.parse(time);
            if ((lTime.isAfter(LocalTime.parse("09:30")) && lTime.isBefore(LocalTime.parse("16:00"))) || lTime.isBefore(LocalTime.parse("07:00")) || lTime.isAfter(LocalTime.parse("19:30"))) {
                return new Response(true, "Can Be on Road !!!");
            } else {
                //Check Valid inputs with regular expressions (Plates)
                if (licencePlateNumber.matches("[A-Z]{3}-[0-9]{4}")) {
                    //Check Valid inputs with regular expressions (Dates)
                    if (date.matches("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")) {
                        int dayOfWeek = LocalDate.parse(date).getDayOfWeek().getValue();
                        ArrayList<Integer> numbersAllowed = daysPlatesNumbers.get(dayOfWeek);
                        if (numbersAllowed.contains(Integer.parseInt(licencePlateNumber.substring(licencePlateNumber.length() - 1)))) {
                            return new Response(false, "Can't Be on Road");
                        } else {
                            return new Response(true, "Can Be on Road !!!");                        }

                    } else {
                        return new Response(false, "Input: Invalid date! , must be format YYYY-mm-DD");
                    }
                } else {
                    return new Response(false, "Input: Invalid licence Plate number! , must be format AAA-0000");
                }

            }

        } else {
            return new Response(false, "Input: Invalid time! , must be format HH:MM");

        }
    }

}
