package com.stackbuilders.picoyplaca;

import com.stackbuilders.picoyplaca.Entity.Response;
import com.stackbuilders.picoyplaca.Process.PicoPlacaImplementation;

/**
 *
 * @author Joel
 */
public class Main {

    public static void main(String[] args) {
        PicoPlacaImplementation picoPlaca = new PicoPlacaImplementation();
        Response response = picoPlaca.canBeOnRoad("PCQ-8341", "2019-12-09", "09:30");
        if (response.isResult()) {
            System.out.println(response);
        } else {
            System.err.println(response);
        }
    }

}
