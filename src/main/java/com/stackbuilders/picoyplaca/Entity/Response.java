package com.stackbuilders.picoyplaca.Entity;

/**
 *
 * @author JREYESR
 */

//Class to print the result..
public class Response {

    private boolean result;
    private String details;

    public Response() {
    }

    public Response(boolean result, String details) {
        this.result = result;
        this.details = details;
    }

    /**
     * @return the result
     */
    public boolean isResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(boolean result) {
        this.result = result;
    }

    /**
     * @return the details
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "Response{" + "result= " + result + ", details= " + details + '}';
    }
    
    

}
